# Getting Started 

Require :

- PHP 8.0.2 or higher and these PHP extensions (which are installed and enabled by default in most PHP 8 installations): Ctype, iconv, PCRE, Session, SimpleXML, and Tokenizer)
- Composer  
- npm or yarn
- Symfony CLI (opitionnal)
- node 8.10 or higher


In Backend folder :


Create an empty database (database_name_example).

-> You'll need an environnement file that  values for the environment variables needed by the app :

you can copy the .env.example rename it .env (should be ignored by Git)  

In this file set at ###> doctrine/doctrine-bundle ###

Uncomment the line and set the connection to the database, with its name (database_name_example), your root and password.

- composer require webapp
- composer require symfony/webpack-encore-bundle
- composer install
- yarn or npm install
- yarn or npm run build (could also run watch)

for the migration :

- php bin/console make:migration
- php bin/console doctrine:migrations:migrate


In frontend folder :

- yarn or npm install
- yarn or npm run build
- yarn or npm start

- symfony server:ca:install
* to avoid CORS request error while fetching in your browser.

Let's Go


### Backoffice

API Documentation :
http://127.0.0.1:8000/api  

To acces Backoffice : 
http://127.0.0.1:8000/register

* Warning: right now every new user will be granted the role of ADMIN by default, the functionnality is not done yet

http://127.0.0.1:8000/login

http://127.0.0.1:8000/admin

